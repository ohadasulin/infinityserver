/**
 * Created by ohad on 2016/04/30.
 */
var express = require('express');
var router = express.Router();
var fs = require('fs');
var Web3 = require('web3');
var demreum = require('./demreum')
var initialized = false;


var ExchangePath = './data/Exchange.contract';
var contractName = 'Exchange';

var rpcNode = 'http://ec2-54-173-23-102.compute-1.amazonaws.com:8541';
// var rpcNode = 'http://ec2-52-86-243-77.compute-1.amazonaws.com:8545';

var web3 = new Web3(new Web3.providers.HttpProvider(rpcNode));
var data = {};
data.users = {};
data.usersByName = {};

data.companies = {};
data.companyByName = {};
data.balances = {};


data.trades = {};


var ExchangeTxt = {};


var exchange;

router.get('/deposit', function (req, res, next) {
    if (req.query.from && req.query.to && req.query.amount && data.users[req.query.from] && data.users[req.query.to] && data.users[req.query.from].amount > req.query.amount) {
        exchange.execute_deposit(function (e, r) {
            if (!e) {
                console.log(r);
                res.send({status: 'deposited'});
            }
        }, [parseInt(req.query.from), parseInt(req.query.to), parseInt(req.query.amount)]);


    } else {
        res.send({error: 'error'});
    }
});

router.get('/getHolderInfo/:id', function (req, res, next) {
    if (data.users[req.params.id]) {
        var ret = {};
        ret.name = data.users[req.params.id].userName;
        ret.cash = data.users[req.params.id].amount;
        ret.isFundManager = data.users[req.params.id].isFundManager;
        // ret.fundManagers = data.managedByCust[req.params.id] ? Object.keys(data.managedByCust[req.params.id]).length :0;
        ret.investors = data.managedCredit[req.params.id] ? Object.keys(data.managedCredit[req.params.id]).length : 0;
        ret.balances = [];
        ret.userInitial = data.users[req.params.id].initial;
        ret.userTotal = data.users[req.params.id].amount;
        Object.keys(data.balances).forEach(function (key) {
            if (data.balances[key][req.params.id]) {
                var last = data.companies[key].data && data.companies[key].data.lastPrice ? data.companies[key].data.lastPrice : 0;
                ret.balances.push({
                    id: key, name: data.companies[key].name, lastPrice: last,
                    amount: data.balances[key][req.params.id], est: Math.round(last * data.balances[key][req.params.id])
                });
                ret.userTotal += Math.round(last * data.balances[key][req.params.id]);
            }

        });

        if (data.managedByCust[req.params.id]) {
            ret.fundManagers = [];
            Object.keys(data.managedByCust[req.params.id]).forEach(function (key) {
                ret.fundManagers.push({
                        id : key,
                        name : data.users[key].userName,
                        amount : data.managed[key][req.params.id] ? data.managed[key][req.params.id] : 0,
                        profit :getFundProfit(key)
                    }
                )
            });
        }


        ret.percentage = Math.round(ret.userTotal / ret.userInitial) * 100 - 100;
        res.send(ret);

    } else {
        res.send({error: 'error'});
    }
});
router.get('/fundManagers', function (req, res, next) {
    var ret =[];
    Object.keys(data.users).forEach(function (key){

        if(data.users[key].isFundManager){
            ret.push({id:data.users[key].id,name :data.users[key].userName, investors:data.managed[key]?data.managed[key]:0,profit : getFundProfit(key) });
        }
    });

    res.send(ret);
});



function getFundProfit(userId) {
    var tot = data.users[userId].amount;
    if (data.managed[userId]) {


        Object.keys(data.managed[userId]).forEach(function (key) {
            if (key != userId) {
                tot += data.managed[userId][key];
            }
        });
    }
    return Math.round(getTotalOfUser(userId)) / tot * 100 - 100;

}

router.get('/companies/add', function (req, res, next) {
    if (req.query.name && !data.companies[req.query.name]) {
        exchange.execute_registerCompany(function (e, r) {
            if (!e) {
                console.log(r);
                res.send({status: 'company registered'});
            }
        }, [req.query.name, req.query.symbol, req.query.mifid, req.query.isin, req.query.ccy, parseInt(req.query.shares), parseInt(req.query.holder)]);
    }
    else {
        res.send({error: 'can\'t register the company'});
    }
});

router.get('/companies/:companyID', function (req, res, next) {
    if (req.params.companyID && data.companies[req.params.companyID]) {
        res.send(data.companies[req.params.companyID]);
    }
    else {
        res.send({error: 'can\'t get the company'});
    }
});

router.get('/companies', function (req, res, next) {


    var totalValue = 0;
    var numHolders = 0;
    var totalVolume = 0;

    for (var currCompany in data.companies) {

        if (data.companies[currCompany].data) {
            totalValue += data.companies[currCompany].data.lastPrice * data.companies[currCompany].shares;

            totalVolume += data.companies[currCompany].data.volume;
        }

        numHolders += Object.keys(data.balances[currCompany]).length;
    }


    var result = {totalValue: totalValue, totalVolume: totalVolume, numHolders: numHolders, companyList: data.companies}
    res.send(result);
    //  res.send(data.companies);

});


router.get('/trades/add', function (req, res, next) {
    if (req.query.from && req.query.to && req.query.amount && req.query.price && req.query.companyId && data.users[req.query.from].amount >= req.query.amount * req.query.price) {
        exchange.execute_addTrade(function (e, r) {
            if (!e) {
                console.log(r);
                res.send({status: 'trade added', response: r});
            }
        }, [parseInt(req.query.from), parseInt(req.query.to), parseInt(req.query.companyId), parseInt(req.query.amount), parseInt(req.query.price)]);
    }
    else {
        res.send({error: 'can\'t add the trade'});
    }
});

router.get('/trades/:tradeID', function (req, res, next) {
    if (req.params.tradeID && data.trades[req.params.tradeID]) {
        res.send(data.trades[req.params.tradeID]);
    }
    else {
        res.send({error: 'trade does not exist'});
    }
});

router.get('/trades', function (req, res, next) {

    res.send(data.trades);

});

// eventCallbacks.userAdded = function (event) {
//     console.log('new user added ' + event.args.numbers[0].c[0] + ' ' + event.args.p1 + 'with amount ' + event.args.numbers[1].c[0]);
//     data.usersByName[event.args.p1] = event.args.numbers[0].c[0];
//     data.users[event.args.numbers[0].c[0]] = {id: event.args.numbers[0].c[0], userName: event.args.p1, amount: event.args.numbers[1].c[0],chainData:getChainData(event)};
//
// };


router.get('/users/add', function (req, res, next) {
    if (req.query.name && req.query.isFund && req.query.address && req.query.amount && !data.usersByName[req.query.name]) {
        exchange.execute_addUser(function (e, r) {
            if (!e) {
                console.log(r);
                res.send({status: 'user added'});
            }
        }, [req.query.address, req.query.name, parseInt(req.query.amount), parseInt(req.query.isFund)]);
    }
    else {
        res.send({error: 'can\'t add the user'});
    }
});

router.get('/users/:userID', function (req, res, next) {
    if (req.params.userID && data.users[req.params.userID]) {
        res.send(data.users[req.params.userID]);
    }
    else {
        res.send({error: 'User does not exist'});
    }
});

router.get('/users', function (req, res, next) {

    res.send(data.users);

});


router.get('/rundemo', function (req, res, next) {
    if (initialized) {
        res.send({status: 'already deployed'});
        return;
    }

    ExchangeTxt = fs.readFileSync('./Exchange.sol', 'utf8');


    var web3 = new Web3(new Web3.providers.HttpProvider(rpcNode));

    var conractAlreadyDeployed = false;

    try {
        fs.accessSync(ExchangePath, fs.F_OK);
        conractAlreadyDeployed = true;
    } catch (e) {

    }

    if (conractAlreadyDeployed) {
        var contents = fs.readFileSync(ExchangePath).toString();
        var cont = JSON.parse(contents);
        exchange = web3.eth.contract(cont.abi).at(cont.address);

        initExchangeListeners();
        res.send({status: 'done'});
    } else {
        var conractSourceExists = false;

        try {
            fs.accessSync('./Exchange.sol', fs.F_OK);
            conractSourceExists = true;
        } catch (e) {

        }

        if (!conractSourceExists) {
            res.send({error: 'no contract source to upload'});
        } else {
            ExchangeTxt = fs.readFileSync('./Exchange.sol', 'utf8');

            demreum.setWeb3(web3);
            demreum.compile(ExchangeTxt, function (resp) {
                demreum.deploy(resp, null, function (e, r) {
                    if (!e) {
                        exchange = r;
                        initialized = true;
                        initExchangeListeners();
                        res.send({status: 'deployed'});
                    }
                });
            });

        }

    }


});

//c TODO
router.get('/data/:type/:id', function (req, res, next) {

    res.send(data[req.params.type][req.params.id]);
});


function initExchangeListeners() {

    if (exchange) {
        exchange.genericEvent({}, {fromBlock: 0, toBlock: 'latest'}).watch(function (error, event) {
            if (!error) {
                if (eventCallbacks[event.args.eventName]) {
                    eventCallbacks[event.args.eventName](event);
                } else {
                    console.log('error event not found. ', event.args.eventName)
                }


            } else {
                console.log('error happened : ', error);
            }
        });
    }
}


var eventCallbacks = {};


eventCallbacks.userAdded = function (event) {
    console.log('new user added ' + event.args.numbers[0].c[0] + ' ' + event.args.p1 + 'with amount ' + event.args.numbers[1].c[0]);
    data.usersByName[event.args.p1] = event.args.numbers[0].c[0];
    data.users[event.args.numbers[0].c[0]] = {
        id: event.args.numbers[0].c[0],
        isFundManager: event.args.numbers[2].c[0] > 0,
        userName: event.args.p1,
        amount: event.args.numbers[1].c[0],
        initial: event.args.numbers[1].c[0],
        chainData: getChainData(event)
    };

};
eventCallbacks.companyAdded = function (event) {
    var compId = parseInt(event.args.numbers[0]);
    var shares = parseInt(event.args.numbers[1]);
    var holder = parseInt(event.args.numbers[2]);

    var compName = event.args.p1;
    var symbol = event.args.p2;
    var mifid = event.args.p3;
    var isin = event.args.p4;


    var company = {
        id: compId,
        name: compName,
        symbol: symbol,
        mifid: mifid,
        isin: isin,
        shares: shares,
        chainData: getChainData(event),
        initialHolder: holder
    };
    data.companies[compId] = company;
    data.companyByName[compName] = compId
    data.balances[compId] = {};
    data.balances[compId][holder] = shares;

};

eventCallbacks.tradeAdded = function (event) {
    var tradeId = parseInt(event.args.numbers[0]);
    var compId = parseInt(event.args.numbers[1]);
    var amount = parseInt(event.args.numbers[2]);
    var price = parseInt(event.args.numbers[3]);
    var from = parseInt(event.args.numbers[4]);
    var to = parseInt(event.args.numbers[5]);
    var trade = {
        id: tradeId,
        compId: compId,
        compName: data.companies[compId].name,
        amount: amount,
        price: price,
        from: from,
        fromName: data.users[from].userName,
        toName: data.users[to].userName,
        to: to,
        chainData: getChainData(event)
    };
    data.trades[tradeId] = trade;
    data.users[from].amount -= trade.amount * trade.price;
    data.users[to].amount += trade.amount * trade.price;
    updateBalance(compId, from);
    updateBalance(compId, to);
    updateCompanyDynamicData(trade);
};

function updateCompanyDynamicData(trade) {
    if (data.companies[trade.compId] && !data.companies[trade.compId].data) {
        var d = {};
        d.low = trade.price;
        d.high = trade.price;
        d.volume = trade.amount;
        d.txs = 0;
        d.trades = [];
        data.companies[trade.compId].data = d;
    } else {
        if (data.companies[trade.compId].data.low > trade.price) {
            data.companies[trade.compId].data.low = trade.price;
        }

        if (data.companies[trade.compId].data.high < trade.price) {
            data.companies[trade.compId].data.high = trade.price;
        }
        data.companies[trade.compId].data.amount += trade.amount;
    }
    data.companies[trade.compId].data.lastTradeTime = trade.chainData.time;
    data.companies[trade.compId].data.change = data.companies[trade.compId].data.lastPrice ?
        ((data.companies[trade.compId].data.lastPrice / trade.price ) * 100 - 100) : 0;
    data.companies[trade.compId].data.lastPrice = trade.price;
    data.companies[trade.compId].data.txs++;
    data.companies[trade.compId].data.trades.push(trade);

}

data.managed = {}
data.managedTotal = {};
data.managedCredit = {};
data.managedByCust = {};


eventCallbacks.depositAdded = function (event) {
    var from = parseInt(event.args.numbers[0]);
    var to = parseInt(event.args.numbers[1]);
    var amount = parseInt(event.args.numbers[2]);

    if (!data.managed[to]) {
        data.managed[to] = {}
        data.managedTotal[to] = {};
        data.managedTotal[to].balances = {};
    }
    if (!data.managedCredit[to]) {
        data.managedCredit[to] = {}
    }
    if (!data.managedByCust[from]) {
        data.managedByCust[from] = {}
    }
    data.managedByCust[from][to] = data.managedByCust[from][to] || 0;
    data.managedByCust[from][to] += amount;

    data.managedCredit[to][from] = data.managedCredit[to][from] || 0;
    data.managedCredit[to][from] + amount;


    var userTotal = getTotalOfUser(to);
    data.users[from].amount -= amount;
    data.managed[to][to] = userTotal;
    data.managed[to][from] = data.managed[to][from] || 0;
    data.managed[to][from] += amount;

    var total = 0;
    Object.keys(data.managed[to]).forEach(function (key) {
        if (data.managed[to][key]) {
            total += data.managed[to][key];
        }
    });
    Object.keys(data.managed[to]).forEach(function (key) {
        if (data.managed[to][key]) {
            data.managedTotal[to][key] = data.managed[to][key] / total;
        }
    });


}

function updateBalance(shareId, userId) {
    exchange.execute_getShareBalance(function (e, d) {
        if (!e) {
            data.balances[shareId][userId] = parseInt(d);
            console.log('updating share balance of ' + data.users[userId].userName + ' to: ' + parseInt(d));
        }
    }, [shareId, userId]);

}

function getTotalOfUser(userId) {
    var userTotal = data.users[userId].amount;
    Object.keys(data.balances).forEach(function (key) {
        if (data.balances[key][userId]) {
            var last = data.companies[key].data && data.companies[key].data.lastPrice ? data.companies[key].data.lastPrice : 0;
            userTotal += Math.round(last * data.balances[key][userId]);
        }
    });
    return userTotal;
}


function getChainData(event) {
    var tx = {};
    tx.blockNumber = event.blockNumber;
    tx.address = event.address;
    tx.blockHash = event.blockHash;
    tx.txHash = event.transactionHash;
    tx.time = new Date(parseInt(event.args.time) * 1000).toLocaleString();
    return tx;
}

function updateFundManager(fundId, userId) {
    if (!data.users[fundId].managed) {
        data.users[fundId].managed = {};
    }
    exchange.execute_getManagedBalance(function (e, r) {
        if (!e) {
            data.users[fundId].managed[userId] = parseInt(r);
            console.log('updating manager data ', data.users[fundId].userName, data.users[userId].userName, r);
        }
    }, [fundId, userId]);
}


module.exports = router;