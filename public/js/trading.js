var app = angular.module('infinityTrading', ['ngRoute', 'ngAnimate']);

app.config(function ($routeProvider) {
    $routeProvider

    // route for the home page
        .when('/portfolio/:portfolioId?', {
            templateUrl: 'pages/index.html'
        })

        // route for the about page
        .when('/companies', {
            templateUrl: 'pages/companies.html',
            controller: 'CompaniesController'
        }).when('/allusers', {
        templateUrl: 'pages/users.html',
        controller: 'UsersController'
    }).when('/funds', {
        templateUrl: 'pages/funds.html',
        controller: 'FundsController'
    }).when('/trades', {
        templateUrl: 'pages/trades.html',
        controller: 'TradesController'
    }).when('/admin', {
            templateUrl: 'pages/admin.html'
        })

        .when('/login', {
            templateUrl: 'pages/login.html'
        })
        .when('/company/:companyId', {
            templateUrl: 'pages/company.html'
        })

      //   .otherwise({
      //       templateUrl: 'pages/index.html'
      // });
});

app.config(function ($locationProvider) {

    $locationProvider.html5Mode(true);
});

app.controller('UsersController', function ($scope, $http) {
    $http.get('/chain/users/').then(function (res) {
        $scope.users = res.data;
    });

    // $scope.users = {"1":{"id":1,"isFundManager":false,"userName":"Ohad","amount":500000,"initial":500000,"chainData":{"blockNumber":857944,"address":"0x21b917183483e694c2320ed46f114a09b4ee09c9","blockHash":"0x35319c31190cd4933fbb13015f8b54313b1e12fc0d2c49900b653019043d7566","txHash":"0xea9e75f43b92237bba35b294d062cc7ab9ee01b13e47f646255e22e3ad2e20df","time":"5/1/2016, 12:27:44 PM"}},"2":{"id":2,"isFundManager":true,"userName":"or","amount":500000,"initial":500000,"chainData":{"blockNumber":857944,"address":"0x21b917183483e694c2320ed46f114a09b4ee09c9","blockHash":"0x35319c31190cd4933fbb13015f8b54313b1e12fc0d2c49900b653019043d7566","txHash":"0xe7f87889a837305b989925ab7b654b3bb5b430470ae51224f9e1bb128b227fb2","time":"5/1/2016, 12:27:44 PM"}},"3":{"id":3,"isFundManager":true,"userName":"yonatan","amount":500000,"initial":500000,"chainData":{"blockNumber":857944,"address":"0x21b917183483e694c2320ed46f114a09b4ee09c9","blockHash":"0x35319c31190cd4933fbb13015f8b54313b1e12fc0d2c49900b653019043d7566","txHash":"0xeb7ac0b8e7e52cd87a2743e4c7fa321fbcdb705c29035bcdf8277f20ed868ae3","time":"5/1/2016, 12:27:44 PM"}},"4":{"id":4,"isFundManager":true,"userName":"Eric Cartman","amount":500000,"initial":500000,"chainData":{"blockNumber":857944,"address":"0x21b917183483e694c2320ed46f114a09b4ee09c9","blockHash":"0x35319c31190cd4933fbb13015f8b54313b1e12fc0d2c49900b653019043d7566","txHash":"0xe838ef941ad5fb9a3586475941b8f0128e7b2c9ad437cddf19c1e57c0e9c18e9","time":"5/1/2016, 12:27:44 PM"}},"5":{"id":5,"isFundManager":true,"userName":"robert","amount":500000,"initial":500000,"chainData":{"blockNumber":857944,"address":"0x21b917183483e694c2320ed46f114a09b4ee09c9","blockHash":"0x35319c31190cd4933fbb13015f8b54313b1e12fc0d2c49900b653019043d7566","txHash":"0x5288e0c60536bc4b1055de35e813462e212c1919aa774983cce13fcfbfeb8836","time":"5/1/2016, 12:27:44 PM"}},"6":{"id":6,"isFundManager":true,"userName":"Barack Obama","amount":500000,"initial":500000,"chainData":{"blockNumber":857944,"address":"0x21b917183483e694c2320ed46f114a09b4ee09c9","blockHash":"0x35319c31190cd4933fbb13015f8b54313b1e12fc0d2c49900b653019043d7566","txHash":"0x3cc4bdf0989f4a492d70a1b819b56f500cdf134ccf698b36bdb04e00474e69ea","time":"5/1/2016, 12:27:44 PM"}}};
});

app.controller('CompaniesController', function ($scope, $http) {
    $http.get('/chain/companies/').then(function (res) {
        $scope.companies = res.data;
    });
    // $scope.companies  = {"totalValue":100000,"totalVolume":50,"numHolders":6,"companyList":{"1":{"id":1,"name":"ICAP","symbol":"ICAP","mifid":"731","isin":"624","shares":5000,"chainData":{"blockNumber":301539,"address":"0xa3af1d44ce200a2347aa9a051f8b67349587d106","blockHash":"0xfed306e0826e00b5dbf38b330689e4d40203185e80a165c885f30e86a5354d82","txHash":"0x88e870edd9daa05b6f8a771e7e86d921bb8cd8c23f2d92addbc181bfa55ba9af","time":"5/1/2016, 11:08:02 AM"},"initialHolder":2,"data":{"low":20,"high":20,"volume":50,"txs":1,"trades":[{"id":1,"compId":1,"compName":"ICAP","amount":50,"price":20,"from":1,"fromName":"ohad","toName":"or","to":2,"chainData":{"blockNumber":301541,"address":"0xa3af1d44ce200a2347aa9a051f8b67349587d106","blockHash":"0xeddf14d6573829faa845fbf4cb7916ab2b1af89a57cdc37e22bf605989ec95f9","txHash":"0x1bd0c7fcfe013eff3fdc3f59a82ac28b4ad17efa091b90fecb598bae99249ae8","time":"5/1/2016, 11:08:06 AM"}}],"lastTradeTime":"5/1/2016, 11:08:06 AM","change":0,"lastPrice":20}},"2":{"id":2,"name":"BlockApps","symbol":"BAPPS","mifid":"438","isin":"4234","shares":5000,"chainData":{"blockNumber":301539,"address":"0xa3af1d44ce200a2347aa9a051f8b67349587d106","blockHash":"0xfed306e0826e00b5dbf38b330689e4d40203185e80a165c885f30e86a5354d82","txHash":"0x453d0b6290a42e8be73cf8f98407a225c93d74c03e470b06f7d457bd424327e3","time":"5/1/2016, 11:08:02 AM"},"initialHolder":3},"3":{"id":3,"name":"IBM","symbol":"IBM","mifid":"653","isin":"987","shares":5000,"chainData":{"blockNumber":301539,"address":"0xa3af1d44ce200a2347aa9a051f8b67349587d106","blockHash":"0xfed306e0826e00b5dbf38b330689e4d40203185e80a165c885f30e86a5354d82","txHash":"0xbce0d2ed21cf9cacbafc7b9750840db4e8934a3cec92b8ce9e93c7fab8932f93","time":"5/1/2016, 11:08:02 AM"},"initialHolder":4},"4":{"id":4,"name":"Microsoft","symbol":"MS","mifid":"764","isin":"87597","shares":5000,"chainData":{"blockNumber":301539,"address":"0xa3af1d44ce200a2347aa9a051f8b67349587d106","blockHash":"0xfed306e0826e00b5dbf38b330689e4d40203185e80a165c885f30e86a5354d82","txHash":"0xe28cc466599c023e48d69f028406758c283125d3503167a00a25de1650c28854","time":"5/1/2016, 11:08:02 AM"},"initialHolder":4},"5":{"id":5,"name":"Deloitte","symbol":"DLT","mifid":"764","isin":"87597","shares":5000,"chainData":{"blockNumber":301539,"address":"0xa3af1d44ce200a2347aa9a051f8b67349587d106","blockHash":"0xfed306e0826e00b5dbf38b330689e4d40203185e80a165c885f30e86a5354d82","txHash":"0xf5e51d45fbc46f4061507c5c41bf3b3257406fc93c04d7b7394bbd3ea9784adf","time":"5/1/2016, 11:08:02 AM"},"initialHolder":4}}};
});

app.controller('TradesController', function ($scope, $http) {
    $http.get('/chain/trades/').then(function (res) {
        $scope.trades = res.data;
    });
    // $scope.trades = {"1":{"id":1,"compId":1,"compName":"company1","amount":50,"price":20,"from":1,"fromName":"ohad","toName":"or","to":2,"chainData":{"blockNumber":284946,"address":"0x8d07946af94058f41a88e29dac1695c1557c201e","blockHash":"0x08a14a2ae8c6bed747ee8b5e0366336c43d0e8c601585765be507053f1d01e94","txHash":"0xbb35b53115c9485636f7d3b6fdb0e1e04e8f07a64e05a096af6cca1983789b09","time":"5/1/2016, 7:18:06 AM"}}};
});

app.controller('MainController', function ($scope, $location) {
    $scope.$location = $location;
    $scope.percentGainLast12m = 37.3;

    $scope.symbols = {
        'IBM': 'https://lh3.googleusercontent.com/-ApNLZ2_15_U/AAAAAAAAAAI/AAAAAAAAIoA/z5XMC3EzZrc/s0-c-k-no-ns/photo.jpg',
        'MS': 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Microsoft_logo.svg/2000px-Microsoft_logo.svg.png',
        'BAPPS': 'http://blockapps.net/images/header_logo_dashboard.png',
        'DLT': 'http://www2.deloitte.com/content/dam/Deloitte/ca/Images/promo_images/about-deloitte-icon.jpg',
        'ICAP': 'http://www.financemagnates.com/wp-content/uploads/fxmag/2013/02/icap-logo.jpg',
        'ETH': 'http://hack.ether.camp/images/bannerLogo.png',
        'HYL': 'https://d1qb2nb5cznatu.cloudfront.net/startups/i/405583-51b2af08233ce42b9e8d04b33e3dddaf-medium_jpg.jpg?buster=1401302957'
    };

    $scope.photos = {
        'Eric Cartman': 'https://upload.wikimedia.org/wikipedia/en/7/77/EricCartman.png',
        'Barack Obama': 'http://www.golocalpdx.com/cache/images/cached/cache/images/remote/http_s3.amazonaws.com/art.golocalpdx.com/News/obama_flag_square_360_360_90.jpg'
    };


    $scope.vol24h = 83923913.2;
    $scope.numShareholders = 923;


});

app.controller('PortfolioController', function ($scope, $routeParams, $http) {
    $scope.formData = {};
    $scope.submitDeposit = function (formData) {
        $http({
            method: 'GET',
            url: '/chain/deposit?from=1&to=' + ($routeParams.portfolioId || 1) + '&amount=' + formData.amount
        });
        $('#myModal').modal('hide');
    };
    
    $http.get('/chain/getHolderInfo/' + ($routeParams.portfolioId || 1)).then(function (res) {
        angular.extend($scope, res.data);
        // $scope.estValue = res.data.balances.reduce(function (prev, next) {
        //     return prev + next.est;        
        // }, res.data.cash);
    });

    // angular.extend($scope, {"name":"ohad","cash":499000,"balances":[{"id":"1","name":"company1","lastPrice":20,"amount":4950,"est":99000}]});



    $scope.aa = $scope.name;
    $scope.numMangers = 3;

    $scope.balances = [];

   
});



app.controller('FundsController', function ($scope, $http) {

    $http.get('/chain/fundManagers').then(function (res) {
        $scope.fundManagers = res.data;
        $scope.numInvestors = res.data.reduce(function (prev, next) {
            return prev + Object.keys(next.investors || {}).length;

        }, 0);
        $scope.avgPercentGainLast12m = res.data.reduce(function (prev, next) {
           return prev + (next.profit || 0);
        }, 0) / (res.data.length || 1);
        
        $scope.numManagers = res.data.length ;
    });
    // $scope.fundManagers = [{
    //    name: 'Super Trader',
    //     investors: 7,
    //     profit: 17
    // },{
    //     name: 'Mega Trader',
    //     investors: 9,
    //     profit: 27
    // },{
    //     name: 'Or Demri',
    //     investors: 0,
    //     profit: -87
    // },{
    //     name: 'Mini Trader',
    //     investors: 2,
    //     profit: 5
    // },{
    //     name: 'Tiny Trader',
    //     investors: 631,
    //     profit: 762
    // }];

    $scope.avgPercentGainLast12m = Math.random()*2 > 1 ? 26 : -15;
});